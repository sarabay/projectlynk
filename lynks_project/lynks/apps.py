from django.apps import AppConfig


class LynksConfig(AppConfig):
    name = 'lynks'
